package com.paowaric.week10;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Rectangle rec = new Rectangle(5, 3);
        System.out.println(rec.toString());
        System.out.printf("%s area: %.3f \n", rec.getName(),rec.calArea());
        System.out.printf("%s perimeter: %.3f \n\n", rec.getName(),rec.calPerimeter());

        Rectangle rec2 = new Rectangle(2, 2);
        System.out.println(rec2.toString());
        System.out.printf("%s area: %.3f \n", rec.getName(),rec2.calArea());
        System.out.printf("%s perimeter: %.3f \n\n", rec.getName(),rec2.calPerimeter());

        Circle circle = new Circle(2);
        System.out.println(circle);
        System.out.printf("%s area: %.3f \n", circle.getName(), circle.calArea());
        System.out.printf("%s perimeter: %.3f \n\n", circle.getName(), circle.calPerimeter());

        Circle circle2 = new Circle(3);
        System.out.println(circle2);
        System.out.printf("%s area: %.3f \n", circle.getName(),circle2.calArea());
        System.out.printf("%s perimeter: %.3f \n\n", circle.getName(),circle2.calPerimeter());

        Triangle triangle = new Triangle(5, 5, 6);
        System.out.println(triangle);
        System.out.printf("%s area: %.3f \n",triangle.getName(), triangle.calArea());
        System.out.printf("%s perimeter: %.3f \n\n", triangle.getName(),triangle.calPerimeter());

        Triangle triangle2 = new Triangle(6, 7, 8);
        System.out.println(triangle2);
        System.out.printf("%s area: %.3f \n",triangle.getName(), triangle2.calArea());
        System.out.printf("%s perimeter: %.3f \n\n", triangle.getName(),triangle2.calPerimeter());
    }
}
